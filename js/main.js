const inputsCheckbox = document.querySelectorAll('.container-custom-checkbox input'),
      ingredients = document.querySelectorAll('.current-pizza-item'),
      drinks = document.querySelectorAll('.select-drink-item'),
      totalAmount = document.querySelector('.total-amount>.summa'),
      orderBtn = document.querySelector('.typical-btn'),
      modalWindow = document.querySelector('.modal-window'),
      submitBtn = document.querySelector('.modal-window__submit-btn');

const subject = document.querySelector('.modal-window__subject'),
      ingredientsSpan = document.querySelector('.modal-window__ingredients'),
      drinksSpan = document.querySelector('.modal-window__drinks');

/* Adding pizza ingredients */

const addIngredients = checkboxes => {
    const nodesArray = Array.from(inputsCheckbox);
    const ingredientsArray = Array.from(ingredients);
    ingredientsArray.splice(0, 2);

    nodesArray.forEach((el, index) => {
        el.addEventListener('click', e => {
            e.target.parentNode.classList.toggle('active');
            ingredientsArray[index].classList.toggle('active');
            calculateOrder();
        });
    });
}

addIngredients(inputsCheckbox);

/* Adding drinks */

const addDrinks = drinkItems => {
    for (let drink of drinkItems) {
        drink.addEventListener('click', e => {
            e.target.parentNode.classList.toggle('active');
            calculateOrder();
        });
    }
}

addDrinks(drinks);

/* Calculate order */

const calculateOrder = () => {
    const ingredients = document.querySelectorAll('.container-custom-checkbox.active');
    const drinks = document.querySelectorAll('.select-drink-item.active');

    const startPrice = 300,
          ingredientsPrice = ingredients.length * 25,
          drinksPrice = drinks.length * 95;

    totalAmount.innerHTML = `${startPrice + ingredientsPrice + drinksPrice}₽`;
}

/* Modal Window */

const prepareWindowModalContent = () => {
    const addedIngredients = document.querySelectorAll('.container-custom-checkbox.active');
    const addedDrinks = document.querySelectorAll('.select-drink-item.active');

    let ingredientList = [];
    if (addedIngredients) {
        for (let ingredient of addedIngredients) {
            ingredientList.push(ingredient.innerText.toLowerCase());
        }
    }
    
    let drinksList = [];
    if (drinksList) {
        for (let drink of addedDrinks) {
            drinksList.push(drink.dataset.name);
        }
    }

    const totalIngredients = ingredientList.join(', ') || '"нет ингредиентов"';
    const totalDrinks = drinksList.join(', ') || '"нет напитков"';
    const totalText = `Вы заказали пиццу с ингредиентами: ${totalIngredients}, а также напитки: ${totalDrinks}, с Вас ${totalAmount.innerHTML}`;

    subject.innerHTML = totalText;
}

orderBtn.addEventListener('click', () => {
    modalWindow.classList.remove('none');
    prepareWindowModalContent();
});

window.addEventListener('click', e => {
    if (e.target === modalWindow) {
        modalWindow.classList.add('none');
    }
});

submitBtn.addEventListener('click', () => {
    modalWindow.classList.add('none');
});

